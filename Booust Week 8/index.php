<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Welcome To Dharma Movies</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="week6.css">

    </head>

    <body>
            <nav class="navbar navbar-inverse navbar-fixed-top">
                    <div class="container">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            
                            <img src="logo.jpg" class="dharmaLogo"> <a class="navbar-brand" href="index.php">DHARMA MOVIES</a>
                        </div>
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav navbar-right">
                                <li><a href="index.php" class="active-nav">Home</a></li>
                                <li><a href="About.php">About</a></li>
                                <li class="dropdown" id="services">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Services<span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="cinemas.php">Cinemas</a></li>
                                        <li><a href="Home-Theatre-Setups.php">Home Theatre Setups</a></li>
                                        <li><a href="movie-rental-gallery.php">Movie Gallery</a></li>
                                    </ul>
                                </li>
                                <li><a href="Contact.php">Contact</a></li>
                                <li class="dropdown" id="signup">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-user"></span> Sign Up<span class="caret"></span></a>
                                        <form id="signUp-form1" action="index.php" class="dropdown-menu" style="width: 350px">
                                                <div class="form-group">
                                                    <label for="firstname">First Name</label>
                                                    <input type="text" class="form-control required" id="firstname" name="firstname" placeholder="enter first name">
                                                </div>
                                                <div class="form-group">
                                                    <label for="lastname">Last Name</label>
                                                    <input type="text" class="form-control required" id="lastname" name="lastname" placeholder="enter last name">
                                                </div>
                                                <div class="form-group required">
                                                    <label>Gender: </label>
                                                    <label for="male">Male</label>
                                                    <input type="radio" name="sex" id="male">
                                                    <label for="female">Female</label>
                                                    <input type="radio" name="sex" id="female">
                                                </div>
                                                <div class="form-group">
                                                    <label for="username">Username</label>
                                                    <input type="text" class="form-control required" id="username" name="username" placeholder="enter username">
                                                </div>


                                                <div class="form-group">
                                                    <label for="email">Email:</label>
                                                    <input type="email" class="form-control required email" id="email" placeholder="Enter email" name="email">
                                                </div>
                                                <div class="form-group">
                                                    <label for="verify_email">Verify Email:</label>
                                                    <input type="email" class="form-control required email" id="verify_email" placeholder="Enter email" name="verify_email">
                                                </div>
                                                <div class="form-group">
                                                    <label for="pwd">Password:</label>
                                                    <input type="password" class="form-control required" id="pwd" placeholder="Enter password" name="pwd">
                                                </div>
                                                <div class="checkbox" style="margin-left: 10px;">
                                                    <label><input type="checkbox" name="accept"> Accept Terms and Conditions</label>
                                                </div>
                                                <button type="submit" class="btn btn-primary" style="margin-left: 10px;">Sign Up</button>
                                                </form>
                                              
                                            </form>
                                        
                                </li>
                                <li class="dropdown" id="logIn">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-log-in"></span> Log-In<span class="caret"></span></a>
                                        <form id="logIn-dropdown" class="dropdown-menu" method="POST" action="/index.php" style="width: 350px">
                                            <div class="form-group">
                                                <label for="username">Username</label>
                                                <input type="text" class="form-control required" id="username" name="username" placeholder="enter username">
                                            </div>
                                            <div class="form-group">
                                                <label for="pwd">Password:</label>
                                                <input type="password" class="form-control required" id="pwd" placeholder="Enter password" name="pwd">
                                            </div>
                                            <div class="checkbox" style="margin-left: 10px;">
                                                <label><input type="checkbox" name="remember"> Remember Me</label>
                                            </div>
                                            <button type="submit" class="btn btn-primary" style="margin-left: 10px;">Log In</button>   
                                        </form>
                                </li>
                                
                            </ul>
                        </div>
                    </div>
                </nav>

    <div class="body-bg">
        <div class="container">
            <div class="jumbotron">
                <h1 style="margin-top: 25px"><strong>WELCOME TO DHARMA MOVIES</strong></h1>
                <p style="font-size: 30px"><b>The Home For Blockbuster Movies.</b></p>
            </div>
        </div>

        <div class="main-body">
            <div class="container">
                        
                <div class="row">  
                    <div class="col-md-12">
                        <div class="jumbotron">

                        <h2><strong>UPCOMING MOVIES</strong></h2>
                        
                            <div id="myCarousel" class="carousel slide" data-ride="carousel">
                                <!-- Indicators -->
                                <ol class="carousel-indicators">
                                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                                <li data-target="#myCarousel" data-slide-to="1"></li>
                                <li data-target="#myCarousel" data-slide-to="2"></li>
                                <li data-target="#myCarousel" data-slide-to="3"></li>
                                <li data-target="#myCarousel" data-slide-to="4"></li>
                                </ol>
                            
                                <!-- Wrapper for slides -->
                                <div class="carousel-inner">
                                <div class="item active">
                                    <img src="Incredibles2.jpg" alt="Los Angeles">
                                </div>
                            
                                <div class="item">
                                    <img src="jurassicWorld.jpg" alt="Chicago">
                                </div>
                            
                                <div class="item">
                                    <img src="miFallout.jpg" alt="New York">
                                </div>

                                <div class="item">
                                        <img src="Skyscraper.jpg" alt="New York">
                                    </div>

                                <div class="item">
                                    <img src="Tag.jpg" alt="New York">
                                </div>
                                </div>
                            
                                <!-- Left and right controls -->
                                <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left"></span>
                                <span class="sr-only">Previous</span>
                                </a>
                                <a class="right carousel-control" href="#myCarousel" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right"></span>
                                <span class="sr-only">Next</span>
                                </a>
                            </div>
                            
                           <div class="slideShow-button">

                           <button type="button" class="btn btn-primary">View more</button>
                           </div>
                        </div>
                    </div>                            
                </div>
            </div>
        </div>

        <div class="main-body">
                <div class="container">
                            
                    <div class="row">  
                        <div class="col-md-12">
                            <div class="jumbotron">
    
                            <h2><strong>AVAILABLE MOVIES FOR RENT</strong></h2>
                            
                                <div id="myCarousel-2" class="carousel slide" data-ride="carousel">
                                    <!-- Indicators -->
                                    <ol class="carousel-indicators">
                                    <li data-target="#myCarousel-2" data-slide-to="0" class="active"></li>
                                    <li data-target="#myCarousel-2" data-slide-to="1"></li>
                                    <li data-target="#myCarousel-2" data-slide-to="2"></li>
                                    <li data-target="#myCarousel-2" data-slide-to="3"></li>
                                    <li data-target="#myCarousel-2" data-slide-to="4"></li>
                                    <li data-target="#myCarousel-2" data-slide-to="5"></li>
                                    </ol>
                                
                                    <!-- Wrapper for slides -->
                                    <div class="carousel-inner">
                                    
                                    <div class="item active">
                                        <img src="uncledrew.jpg" alt="Los Angeles">
                                    </div>

                                    <div class="item">
                                        <img src="Incredibles2.jpg" alt="Los Angeles">
                                    </div>
                                
                                    <div class="item">
                                        <img src="jurassicWorld.jpg" alt="Chicago">
                                    </div>
                                
                                    <div class="item">
                                        <img src="miFallout.jpg" alt="New York">
                                    </div>
    
                                    <div class="item">
                                            <img src="Skyscraper.jpg" alt="New York">
                                        </div>
    
                                    <div class="item">
                                        <img src="Tag.jpg" alt="New York">
                                    </div>
                                    </div>
                                
                                    <!-- Left and right controls -->
                                    <a class="left carousel-control" href="#myCarousel-2" data-slide="prev">
                                    <span class="glyphicon glyphicon-chevron-left"></span>
                                    <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="right carousel-control" href="#myCarousel-2" data-slide="next">
                                    <span class="glyphicon glyphicon-chevron-right"></span>
                                    <span class="sr-only">Next</span>
                                    </a>
                                </div>
                                
                               <div class="slideShow-button">
    
                               <button type="button" class="btn btn-primary">View more</button>
                               </div>
                            </div>
                        </div>                            
                    </div>
                </div>
            </div>
    </div>

                <div class="footer-section">
                <nav class="navbar navbar-inverse navbar-static-bottom" id="footer-navbar">
                    <div class="container">
                        <div class="navbar-header">
                           <a class="navbar-brand" href="index.html">DHARMA MOVIES 2018</a>
                        </div>
                        <div>
                            <ul class="nav navbar-nav navbar-right">
                                <li><a href="#">Careers</a></li>
                                <li><a href="#">Donate</a></li>
                                <li><a href="#">Events</a></li>
                                <li><a href="#">Advertise With Us</a></li>
                            </ul>
                        </div>
                    </div>
                </nav>  
            </div>





        <script type="text/javascript" src="jquery-3.3.1.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <script type="text/javascript" src="custom_jquery.js"></script>   
    </body>
    </html>