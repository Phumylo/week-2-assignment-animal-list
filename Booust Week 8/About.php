<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Dharma Movies About Us</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="week6.css">

    </head>

    <body>
            <nav class="navbar navbar-inverse navbar-fixed-top">
                    <div class="container">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            
                            <img src="logo.jpg" class="dharmaLogo"> <a class="navbar-brand" href="index.php">DHARMA MOVIES</a>
                        </div>
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav navbar-right">
                                <li><a href="index.php">Home</a></li>
                                <li><a href="About.php" class="active-nav">About</a></li>
                                <li class="dropdown" id="services">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Services<span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="cinemas.php">Cinemas</a></li>
                                        <li><a href="Home-Theatre-Setups.php">Home Theatre Setups</a></li>
                                        <li><a href="movie-rental-gallery.php">Movie Gallery</a></li>
                                    </ul>
                                </li>
                                <li><a href="Contact.php">Contact</a></li>
                                <li class="dropdown" id="signup">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-user"></span> Sign Up<span class="caret"></span></a>
                                    <form id="signUp-form1" action="index.php" class="dropdown-menu" style="width: 350px">
                                            <div class="form-group">
                                                <label for="firstname">First Name</label>
                                                <input type="text" class="form-control required" id="firstname" name="firstname" placeholder="enter first name">
                                            </div>
                                            <div class="form-group">
                                                <label for="lastname">Last Name</label>
                                                <input type="text" class="form-control required" id="lastname" name="lastname" placeholder="enter last name">
                                            </div>
                                            <div class="form-group required">
                                                <label>Gender: </label>
                                                <label for="male">Male</label>
                                                <input type="radio" name="sex" id="male">
                                                <label for="female">Female</label>
                                                <input type="radio" name="sex" id="female">
                                            </div>
                                            <div class="form-group">
                                                <label for="username">Username</label>
                                                <input type="text" class="form-control required" id="username" name="username" placeholder="enter username">
                                            </div>


                                            <div class="form-group">
                                                <label for="email">Email:</label>
                                                <input type="email" class="form-control required email" id="email" placeholder="Enter email" name="email">
                                            </div>
                                            <div class="form-group">
                                                <label for="verify_email">Verify Email:</label>
                                                <input type="email" class="form-control required email" id="verify_email" placeholder="Enter email" name="verify_email">
                                            </div>
                                            <div class="form-group">
                                                <label for="pwd">Password:</label>
                                                <input type="password" class="form-control required" id="pwd" placeholder="Enter password" name="pwd">
                                            </div>
                                            <div class="checkbox" style="margin-left: 10px;">
                                                <label><input type="checkbox" name="accept"> Accept Terms and Conditions</label>
                                            </div>
                                            <button type="submit" class="btn btn-primary" style="margin-left: 10px;">Sign Up</button>
                                            </form>
                                          
                                        </form>
                                    
                            </li>
                            <li class="dropdown" id="logIn">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-log-in"></span> Log-In<span class="caret"></span></a>
                                    <form id="logIn-dropdown" class="dropdown-menu" method="POST" action="/action_page.php" style="width: 350px">
                                        <div class="form-group">
                                            <label for="username">Username</label>
                                            <input type="text" class="form-control required" id="username" name="username" placeholder="enter username">
                                        </div>
                                        <div class="form-group">
                                            <label for="pwd">Password:</label>
                                            <input type="password" class="form-control required" id="pwd" placeholder="Enter password" name="pwd">
                                        </div>
                                        <div class="checkbox" style="margin-left: 10px;">
                                            <label><input type="checkbox" name="remember"> Remember Me</label>
                                        </div>
                                        <button type="submit" class="btn btn-primary" style="margin-left: 10px;">Log In</button>   
                                    </form>
                            </li>
                                
                            </ul>
                        </div>
                    </div>
                </nav>

    <div class="body-bg">
        <div class="container">
                <div class="jumbotron">
                        <h1 style="margin-top: 25px"><strong>ABOUT US</strong></h1>
                </div>

                <div class="jumbotron AB-JBT-1" style="height: 400px">
                    <div class="about-body">
                        <h2 style="margin-bottom: 20px"><b>WHO ARE WE?</b></h2>
                        <img src="theatre.jpg" class="outdoorTheatre img-responsive pull-left img-thumbnail" style="width: 300px; height: 250px; margin-bottom: 20px">
                        
                        <p style="text-align: justify">We are an indigenous movie rental company situated in the Centre of Excellence, Lagos State.
                            We have been in the industry of movie entertainment for Nigerians for 3 years, yet we are just getting started. Our primary goal is to
                            provide Nigerian movie lovers an opportunity to indulge in top quality movie entertaintment of their choice.
                        </p>
                    </div>
                </div>
        </div>
    </div>

            <div class="footer-section">
                <nav class="navbar navbar-inverse navbar-static-bottom" id="footer-navbar">
                    <div class="container">
                        <div class="navbar-header">
                           <a class="navbar-brand" href="index.php">DHARMA MOVIES 2018</a>
                        </div>
                        <div>
                            <ul class="nav navbar-nav navbar-right">
                                <li><a href="#">Careers</a></li>
                                <li><a href="#">Donate</a></li>
                                <li><a href="#">Events</a></li>
                                <li><a href="#">Advertise With Us</a></li>
                            </ul>
                        </div>
                    </div>
                </nav>  
            </div>
            
           


        <script src="jquery-3.3.1.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="custom_jquery.js"></script>   
    </body>
    </html>