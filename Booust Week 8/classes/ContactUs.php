<?php

class ContactUs{
    public static function process($name, $email, $comment){

        $file = fopen( 'contact.csv', 'a');

        fputcsv($file, array('Name', 'Email', 'Message', 'Date'));

        fputcsv($file, array($name, $email, $comment, date('d-m-Y H:i:s')));

        fclose($file);

        $message = "Hello " .$name. ",\n\nThank you for contacting us. We will respond to you shortly.\n\nRegards.";
        $headers = "Reply-To: Dharma Admin <admin@dharmamovies.com>\r\n";
        $headers .= "Return-Path: Dharma Admin <<admin@dharmamovies.com>\r\n";
        $headers .= "From: Dharma Admin <admin@dharmamovies.com>\r\n";


       return mail($email, 'Dharma Contact', $message, $headers);
    }
}

?>