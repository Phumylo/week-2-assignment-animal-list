<!DOCTYPE html>
<html>
    <body>

        <?php
        $yr = 1979;

        $counter = 0;

        while($yr < 2018) {

            $yr++;
            
            if($yr % 4 < 1 && $yr % 100 > 0 || $yr % 400 < 1) {
                $counter = $counter + 1;
                echo $yr . 'Leap Year' . "<br>";
            }
            else {
                echo $yr . "<br>";
            }
        }
        
        echo "<br>" . "Total number of Leap Years: " . $counter;



        ?>

    </body>
</html>