$(document).ready(function(){
   
    $("#display-theatres").hide();

    $("#display-theatres").click(function(){
        $(".row-1").show(1000);
        $(".row-2").show(1000);
        $("#display-theatres").hide();
        $("#hide-theatres").show();
    })

    $("#hide-theatres").click(function(){
        $(".row-1").hide(1000);
        $(".row-2").hide(1000);
        $("#hide-theatres").hide();
        $("#display-theatres").show();
    })

    //Creating a slide down animation for Services Dropdown menu.
    $("#services .dropdown-toggle").click(function(){
        $("#services .dropdown-menu").slideToggle(600);
        $("#signup .dropdown-menu").slideUp(100);
        $("#logIn .dropdown-menu").slideUp(100);
    })

    //Creating a slide down animation for Signup Dropdown form.
    $("#signup .dropdown-toggle").click(function(){
        $("#signup .dropdown-menu").slideToggle(600);
        $("#services .dropdown-menu").slideUp(100);
        $("#logIn .dropdown-menu").slideUp(100);
    })

    
    $("#signup input").focus(function(){
        $(this).css("background-color", "#a2a7aa");
    })
    $("#signup input").blur(function(){
        $(this).css("background-color", "white");
    })
    $("#signup input").hover(function(){
        $(this).css("border-color", "skyblue");},
        function(){
            $(this).css("border-color", "inherit");
    })

    //Creating a slide down animation for logIn Dropdown form.
    $("#logIn .dropdown-toggle").click(function(){
        $("#logIn .dropdown-menu").slideToggle(600);
        $("#services .dropdown-menu").slideUp(100);
        $("#signup .dropdown-menu").slideUp(100);
    })


    $("#logIn input").focus(function(){
        $(this).css("background-color", "#a2a7aa");
    })
    $("#logIn input").blur(function(){
        $(this).css("background-color", "white");
    })
    $("#logIn input").hover(function(){
        $(this).css("border-color", "skyblue");},
        function(){
            $(this).css("border-color", "inherit");
    })

    $(".jumbotron, .body-bg").click(function(){
        $("#services .dropdown-menu").slideUp(100);
        $("#signup .dropdown-menu").slideUp(100);
        $("#logIn .dropdown-menu").slideUp(100); 
    })

    $(".AB-JBT-1").hide();
    $(".AB-JBT-1").slideDown(1000);

    $("#signUp-form1").validate({
        rules: {
            verify_email: {
                required: true,
                email: true,
                equalTo: "#email"
            }
        }
    });


    $("#logIn-dropdown").validate();
});

